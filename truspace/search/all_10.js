var searchData=
[
  ['r_170',['R',['../classHamiltonian.html#a77d8542358120deb5f71feadeba49524',1,'Hamiltonian']]],
  ['r_171',['r',['../classminimal__model__parameters.html#a177464f1cbd21a99346b5563319645f9',1,'minimal_model_parameters']]],
  ['r_5fop_172',['r_op',['../classminimal__model__parameters.html#a1e41fcaf7a525dbce6370912ed74ba1c',1,'minimal_model_parameters']]],
  ['read_5fchiral_173',['read_chiral',['../classminimal__model__states.html#a9e2e63e6b48e43cc4614b14e33f2d169',1,'minimal_model_states']]],
  ['read_5fme_174',['READ_ME',['../classminimal__model__parameters.html#a9594fe23e5804dc552f5d87b86327bd2',1,'minimal_model_parameters']]],
  ['read_5fstates_175',['READ_STATES',['../classminimal__model__parameters.html#a29b806f7933a5e8a06293a0c740e7832',1,'minimal_model_parameters']]],
  ['rinc_176',['rInc',['../classminimal__model__parameters.html#a1b4584414f90bbce65719e9014a3abae',1,'minimal_model_parameters::rInc()'],['../classcustom__parameters.html#a1662b9c05022c1638b4d10da38fbcbda',1,'custom_parameters::rInc()']]],
  ['rowh_177',['rowH',['../classHamiltonian.html#a8d52518ef74eb8cff73d375d1e986c82',1,'Hamiltonian']]],
  ['rstart_178',['rStart',['../classminimal__model__parameters.html#a55e074cacce63a3b3732b1de8c07ec6f',1,'minimal_model_parameters::rStart()'],['../classcustom__parameters.html#ac90284ed19efa79129ebfcf2e6440b1e',1,'custom_parameters::rStart()']]],
  ['rsteps_179',['rSteps',['../classminimal__model__parameters.html#a2e9948fbb9c95a1da11517aa49e689a1',1,'minimal_model_parameters::rSteps()'],['../classcustom__parameters.html#aece9b5be942356266aebb6058234d49b',1,'custom_parameters::rSteps()']]],
  ['run_180',['run',['../classNRG.html#a0cdebccf7b1db2cd43c5516dbc4a2d99',1,'NRG::run(parameters &amp;p, Hamiltonian &amp;H, filenames &amp;fnames)'],['../classNRG.html#a0cdebccf7b1db2cd43c5516dbc4a2d99',1,'NRG::run(parameters &amp;p, Hamiltonian &amp;H, filenames &amp;fnames)'],['../classworker.html#a456282fe869b1e59de35d7bd0958f981',1,'worker::run()']]]
];
